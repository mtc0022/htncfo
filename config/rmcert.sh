#!/bin/bash
# Bash Script to remove a cert given a serial number
# Syntax:
# ./rm-cert CERT
openssl ca -revoke "$1"
