#!/bin/bash
# Bash Script to make a Cert from a CSR
# Syntax:
# ./mk-cert USERNAME 
# Send the CSR in via stdin

# If no USERNAME
if [ -z $1  ]
then
	echo USAGE: new-cert USERNAME \n Pipe the CSR via stdin
	exit -1
fi

# Grab the CSR PIPE
if [ -p /dev/stdin ];
then
	# Grab stdin
	PIPE=`cat`
fi

#echo "$PIPE"
# Step Made by CA
# Client Cert, by default this will not be able to get revoked
echo "$PIPE" | openssl x509 -req -CA /srv/acm/config/ca/ca.crt -CAkey /srv/acm/config/ca/ca.key  -out "/srv/acm/store/crt/$1.crt" -days 90

