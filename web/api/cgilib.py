# Python library for generic CGI operations.
# Do not make this lib an executable file. It is only meant to provide common cgi functions
from os import environ
from http.client import HTTPSConnection,HTTPConnection
from urllib.parse import parse_qs

# Headers
def PrintResponse(status,content='text/plain',headers=dict()):
   content_type = 'Content-type: '+content+'; charset=UTF-8\n'
   lines = '\n'.join([k+':'+v for k,v in headers.items()])
   st = 'Status: '+status+'\n'
   print("HTTP/1.0 \n"+st+content_type+lines)

# Global resp to hold data to be printed
resp = ''
def sprint(s=''):
    global resp
    resp += str(s)+'\n'
    return resp

# Turn cookies into a dictornary
def GetCookies()->dict:
    if 'HTTP_COOKIE' in environ :
        cookiestr = environ['HTTP_COOKIE']
        cookies = {k:v for k,v in [ cookie.split('=') for cookie in cookiestr.split(';')] }
        return cookies
    return {}
        

